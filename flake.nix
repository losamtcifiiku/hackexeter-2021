{
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        yarn2 = pkgs.stdenv.mkDerivation {
          name = "yarn2";
          src = ./.;
        };
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ heroku git git-lfs nodejs yarn ];
        };

        defaultPackage = pkgs.mkYarnPackage {
          name = "corona-walk";
          src = ./.;
          packageJSON = ./package.json;
          yarnLock = ./yarn.lock;
          dontStrip = true;
        };

        defaultApp = flake-utils.lib.mkApp { drv = defaultPackage; };
      });
}
