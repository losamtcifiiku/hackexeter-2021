import {
  Person,
  CrossroadsMap,
  PathfindRequest,
  PathfindResponse,
  Point,
  isPathfindRequest,
  isArray,
  isPoint,
} from "../common";

function drawPath(ctx: CanvasRenderingContext2D, lines: Array<Point>): void {
  ctx.strokeStyle = "red";
  ctx.lineWidth = 5;

  for (let i = 1; i < lines.length; i++) {
    ctx.beginPath();
    ctx.moveTo(lines[i - 1].x, lines[i - 1].y);
    ctx.lineTo(lines[i].x, lines[i].y);
    ctx.stroke();
  }
}

function drawMap(ctx: CanvasRenderingContext2D, map: CrossroadsMap) {
  for (const road of map.roads) {
    const from = map.crossroads[road.from];
    const to = map.crossroads[road.to];

    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(from.position.x, from.position.y);
    ctx.lineTo(to.position.x, to.position.y);
    ctx.stroke();

    ctx.fillStyle = "red";
    for (const person of road.people) {
      ctx.beginPath();
      ctx.arc(person.position.x, person.position.y, 5, 0, 2 * Math.PI);
      ctx.fill();
    }
  }

  ctx.fillStyle = "black";
  for (const crossroads of map.crossroads) {
    ctx.beginPath();
    ctx.arc(crossroads.position.x, crossroads.position.y, 5, 0, 2 * Math.PI);
    ctx.fill();
  }
}

async function pathfind(request: PathfindRequest): Promise<PathfindResponse> {
  const response = await fetch("/pathfind", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(request),
  });

  const json = await response.json();

  if (!isArray(json, isPoint)) {
    console.error(`${JSON.stringify(json)} is not array of points`);
    return null;
  }

  return json;
}

async function onFileLoaded(this: FileReader) {
  if (typeof this.result !== "string") {
    return;
  }

  const json = JSON.parse(this.result);

  if (!isPathfindRequest(json)) {
    console.error("Got malformed pathfind request");
    return;
  }

  const request = json as PathfindRequest;

  const canvas = document.querySelector("#map");
  if (!(canvas instanceof HTMLCanvasElement)) {
    return;
  }

  const ctx = canvas.getContext("2d");
  if (ctx === null) {
    return;
  }

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawMap(ctx, request.map);

  const path = await pathfind(request);
  if (path !== null) {
    drawPath(ctx, path);
  }
}

function onFileUploaded(this: HTMLInputElement) {
  const reader = new FileReader();

  if (this.files === null || this.files.length === 0) {
    return;
  }

  reader.onload = onFileLoaded;
  reader.readAsText(this.files[0]);
}

const query_file = document.querySelector("#json-map");

if (!(query_file instanceof HTMLInputElement) || query_file.type !== "file") {
  console.error(`${query_file} is not file input`);
} else {
  query_file.addEventListener("change", onFileUploaded);
}
