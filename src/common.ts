export function isBoolean(val: any): boolean {
  return typeof val === "boolean";
}

export function isNumber(val: any): boolean {
  return typeof val === "number";
}

export function isArray(vals: any, elemPred?: (_: any) => boolean): boolean {
  if (!(vals instanceof Array)) return false;

  if (elemPred) {
    for (const val of vals) {
      if (!elemPred(val)) {
        return false;
      }
    }
  }

  return true;
}

export function isObject(val: any, ...keys: string[]): boolean {
  if (typeof val !== "object") return false;

  for (const key of keys) {
    if (!(key in val)) {
      return false;
    }
  }

  return true;
}

export type Either<L, R> = { ok: false; error: L } & { ok: true; value: R };

export function isEither(
  val: any,
  leftPred: (_: any) => boolean,
  rightPred: (_: any) => boolean
): boolean {
  return (
    isObject(val, "ok") &&
    isBoolean(val.ok) &&
    (val.ok
      ? "value" in val && rightPred(val.value)
      : "error" in val && leftPred(val.error))
  );
}

export type Point = {
  x: number;
  y: number;
};

export function isPoint(val: any) {
  return isObject(val, "x", "y");
}

export type Person = {
  position: Point;
  hasMask: boolean;
  hasCorona: boolean;
};

export function isPerson(val: any) {
  return (
    isObject(val, "position", "hasMask", "hasCovid") &&
    isPoint(val.position) &&
    isBoolean(val.hasMask) &&
    isBoolean(val.hasCovid)
  );
}

export type Road = {
  people: Array<Person>;
  from: number;
  to: number;
};

export function isRoad(val: any): boolean {
  return (
    isObject(val, "people", "from", "to") &&
    isArray(val.people, isPerson) &&
    isNumber(val.from) &&
    isNumber(val.to)
  );
}

export type Crossroad = {
  position: Point;
};

export function isCrossroad(val: any): boolean {
  return isObject(val, "position") && isPoint(val.position);
}

export type CrossroadsMap = {
  crossroads: Array<Crossroad>;
  roads: Array<Road>;
};

export function isCrossroadsMap(val: any): boolean {
  return (
    isObject(val, "crossroads", "roads") &&
    isArray(val.crossroads, isCrossroad) &&
    isArray(val.roads, isRoad)
  );
}

export type PathfindRequest = {
  from: number;
  to: number;
  safetyCoeff: number;
  map: CrossroadsMap;
};

export function isPathfindRequest(val: any): boolean {
  return (
    isObject(val, "from", "to", "safetyCoeff", "map") &&
    isNumber(val.from) &&
    isNumber(val.to) &&
    isNumber(val.safetyCoeff) &&
    isCrossroadsMap(val.map)
  );
}

export type PathfindResponse = Array<Point> | null;

export function isPathfindResponse(val: any): boolean {
  return val === null || isArray(val, isPoint);
}

export const PointSchema = {
  type: "object",
  properties: { x: { type: "number" }, y: { type: "number" } },
  required: ["x", "y"],
} as const;

export const PersonSchema = {
  type: "object",
  properties: {
    position: PointSchema,
    hasMask: { type: "boolean" },
    hasCorona: { type: "boolean" },
  },
  required: ["position", "hasMask", "hasCorona"],
} as const;

export const RoadSchema = {
  type: "object",
  properties: {
    people: { type: "array", items: PersonSchema },
    from: { type: "number" },
    to: { type: "number" },
  },
  required: ["people"],
} as const;

export const CrossroadsSchema = {
  type: "object",
  properties: {
    position: PointSchema,
  },
} as const;

export const MapSchema = {
  type: "object",
  people: {
    type: "object",
    properties: {
      crossroads: {
        type: "array",
        items: CrossroadsSchema,
      },
      roads: {
        type: "array",
        items: RoadSchema,
      },
    },
  },
} as const;

export const PathfindRequestSchema = {
  type: "object",
  properties: {
    from: { type: "number" },
    to: { type: "number" },
    safetyCoeff: { type: "number" },
    map: MapSchema,
  },
  required: ["from", "to", "safetyCoeff", "map"],
} as const;

export const PathfindResponseSchema = {
  type: "array",
  items: PointSchema,
} as const;

export const PathfindSchema = {
  body: PathfindRequestSchema,
  response: {
    200: PathfindResponseSchema,
    500: { type: "null" },
  },
} as const;
